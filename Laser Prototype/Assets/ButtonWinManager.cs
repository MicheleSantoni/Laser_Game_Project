﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonWinManager : MonoBehaviour {


    public void ReturnMenuStart()
    {
        SceneManager.LoadScene("ProvaUI2");
    }

    public void Restart()
    {

    }

    public void NextLevel()
    {
        if(SceneManager.GetActiveScene().buildIndex + 1 < SceneManager.sceneCountInBuildSettings) 
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            Debug.Log("We are inside next scene if");
        }
    }
}

