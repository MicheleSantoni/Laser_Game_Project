﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationSphere : MonoBehaviour {

    public LevelCreate level;
    public Transform target;
    public float speed = 0;
    Rigidbody rig;
    // Use this for initialization
    void Start () {
        transform.position = new Vector3(target.transform.position.x + (level.size_grid_lun / 2f) - 0.5f, target.transform.position.y + (level.size_grid_alt / 2f), target.transform.position.z + (level.size_grid_prof / 2f) - 0.5f);
     
       
    }

    // Update is called once per frame
    void Update()
    {
        
        Rotation();
        
    }
    void Rotation()
    {
        Vector3 move = Vector3.zero;
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            move += Vector3.up;

        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            move += Vector3.down;
        }

         if (Input.GetKey(KeyCode.UpArrow))
        {
            move += Vector3.right;

        }

         if (Input.GetKey(KeyCode.DownArrow))
        {
            move += Vector3.left;

        }

        transform.Rotate(move * speed * Time.deltaTime);     //RotateAround(transform.position, move, speed * Time.deltaTime);
    }
}
