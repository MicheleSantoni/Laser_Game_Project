﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activate_Laser : MonoBehaviour {

    public LineRenderer laser_to_activate;
    public GameObject laser_shooter;

	// Use this for initialization
	void Start () {
        

        Deactivate();
	}

    public void Activate()
    {
        //laser_to_activate.SetPosition(0, laser_shooter.transform.position);
        //laser_to_activate.SetPosition(1, laser_shooter.transform.right * 100f);

        laser_to_activate.enabled = true;
    }
    public void Deactivate()
    {
        laser_to_activate.enabled = false;
    }
}
