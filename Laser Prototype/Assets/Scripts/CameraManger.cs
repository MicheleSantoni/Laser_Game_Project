﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManger : MonoBehaviour {
    public Transform target;
    public float horizMove = 10f;
     public float vertMove = 10f;

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame

    private void Update()
    {
        //if (GameObject.Find("Game Controller").GetComponent<Game_Control>().action == Game_Control.Actions.POSIZIONAMENTO)
        //{
        //    if (Input.GetKey(KeyCode.A))
        //    {

        //        MoveHorizontal(true);
        //    }
        //    else if (Input.GetKey(KeyCode.D))
        //    {

        //        MoveHorizontal(false);
        //    }
        //    if (Input.GetKey(KeyCode.W))
        //    {

        //        MoveVertical(false);
        //    }
        //    else if (Input.GetKey(KeyCode.S))
        //    {

        //        MoveVertical(true);
        //    }
        //}
    }

    public void MoveHorizontal(bool left)
    {
        float dir = 1;
        if (!left)
        {
            dir *= -1;
            transform.RotateAround(target.position, Vector3.up, horizMove * dir);
        }
        else
        {
            transform.RotateAround(target.position, Vector3.up, horizMove * dir);
        }
    
    }

    public void MoveVertical(bool up)
    {
        float dir = -1;
        if (!up)
            dir *= -1;
        transform.RotateAround(target.position, transform.TransformDirection(Vector3.right), vertMove * dir * Time.deltaTime);
    }
}
