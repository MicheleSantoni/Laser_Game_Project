﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Controller : MonoBehaviour
{

    //public float distance;
    public LevelCreate level;
    public float RotationVelocity;
    public Transform target;
    public Player_Commands commands;
    //public float vert_sensib, horiz_sensib;
    //public float max_y_angle, max_x_angle;
    //Vector2 current_rotation;
    //int altezza;
    //public int spostamentoAltezza;
    //float startAltezza;
    bool isMoving;
    public float trigger_dead = 0.1f;


    // Use this for initialization
    void Start()
    {
        isMoving = false;
        //distance = Vector3.Distance(transform.position, target.transform.position);
        //distance = 20f;
        //altezza = 0;
        //startAltezza = transform.localRotation.eulerAngles.x;
        transform.localPosition = new Vector3((level.size_grid_lun / 2f) - 0.5f, (level.size_grid_alt / 2f), (level.size_grid_prof / 2f) - 0.5f);
    }

    void Update()
    {
        Rotate_Camera();
    }

    public void Rotate_Camera()
    {
        if (target)
        {
            /*
            //transform.LookAt(target);
            if (Input.GetMouseButton(1))
            {
                print("Pressed");
                current_rotation.x -= Input.GetAxis("Mouse Y") * vert_sensib;
                current_rotation.y += Input.GetAxis("Mouse X") * horiz_sensib;

                current_rotation.x = Mathf.Clamp(current_rotation.x, -max_x_angle, max_x_angle);

                transform.rotation = Quaternion.Euler(current_rotation.x, current_rotation.y, 0);
            }
            */
            if (!isMoving)
            {
                if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(commands.Right_Bumper))
                {
                    StartCoroutine(GoTo(-90f));
                    
                    //print("Destra");
                }

                else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(commands.Left_Bumper))
                {
                    StartCoroutine(GoTo(90f));
                    //print("sinistra");
                }
                else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetAxis("Left Trigger") > trigger_dead)
                {
                    StartCoroutine(GoUpDown(-90f));
                    //print("giu");
                }
                else if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetAxis("Right Trigger") > trigger_dead)
                {
                    StartCoroutine(GoUpDown(90f));
                    //print("su");
                }
            }
        }

    }


    IEnumerator GoTo(float rotation)
    {

        //float startRotation = transform.localRotation.y;

        Vector3 eulerRot = transform.rotation.eulerAngles;
        float startRotation = eulerRot.y;

        float i = 0;
        isMoving = true;
        while (i < RotationVelocity)
        {

            transform.Rotate(0, rotation / RotationVelocity, 0);
            //Quaternion.Lerp(Quaternion.Euler(transform.rotation.x, startRotation, transform.rotation.z),Quaternion.Euler(transform.rotation.x, transform.rotation.y + rotation, transform.rotation.z), RotationVelocity*i);
            //gameObject.transform.localRotation = Quaternion.Euler(gameObject.transform.localRotation.eulerAngles.x, startRotation + (rotation / RotationVelocity) * i, gameObject.transform.localRotation.eulerAngles.z);
            //print(gameObject.transform.localRotation.eulerAngles.y);
            i++;
            
            yield return null;
        }

        isMoving = false;
    }

    IEnumerator GoUpDown(float rotation)
    {

        Vector3 eulerRot = transform.rotation.eulerAngles;
        float startRotation = eulerRot.x;

        float i = 0;
        isMoving = true;
        while (i < RotationVelocity)
        {

            transform.Rotate(rotation / RotationVelocity, 0, 0);
            //Quaternion.Lerp(Quaternion.Euler(transform.rotation.x, startRotation, transform.rotation.z),Quaternion.Euler(transform.rotation.x, transform.rotation.y + rotation, transform.rotation.z), RotationVelocity*i);
            //gameObject.transform.localRotation = Quaternion.Euler(startRotation + (rotation / RotationVelocity) * i,gameObject.transform.localRotation.eulerAngles.y, gameObject.transform.localRotation.eulerAngles.z);
            //print(gameObject.transform.localRotation.eulerAngles.y);
            i++;

            yield return null;
        }
        isMoving = false;

        /* int start = altezza;

         altezza += direzione;

         altezza = Mathf.Clamp(altezza, -1, 1); */

        /*
        Transform startRotation = transform;
        if (start != altezza)
        {
            isMoving = true;
            int i = 0;
            while (i <= RotationVelocity)
            {
                //transform.localRotation = Quaternion.Euler(startAltezza + (spostamentoAltezza * altezza) * i / RotationVelocity, transform.localRotation.eulerAngles.y, transform.localRotation.eulerAngles.z);
                // transform.localRotation = Quaternion.Euler(startAltezza + (spostamentoAltezza * altezza) * i / RotationVelocity, transform.localRotation.eulerAngles.y, transform.localRotation.z);
                gameObject.transform.rotation = Quaternion.Euler(startRotation.transform.localRotation.eulerAngles.x + spostamentoAltezza * direzione * i / RotationVelocity /3, transform.localRotation.eulerAngles.y, transform.localRotation.eulerAngles.z);
                i++;
                yield return null;
            }
            //startAltezza = transform.localRotation.eulerAngles.x;
        }

        isMoving = false;
        */


    }

}

