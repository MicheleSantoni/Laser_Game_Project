﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Change_Camera : MonoBehaviour {

    public Player_Commands commands;
    public Camera main_camera, free_camera;
    public GameObject main_controller,free_controller;
    bool main_cam_ctrl, free_cam_ctrl;

    private void Awake()
    {
        main_camera.enabled = true;
        main_controller.GetComponent<Camera_Controller>().enabled = true;
        free_camera.enabled = false;
        free_controller.GetComponent<RotationSphere>().enabled = false;

    }

    // Update is called once per frame
    void Update () {

        Swap_Camera();
	}

    void Swap_Camera()
    {
        if (Input.GetKeyDown(commands.change_camera) || Input.GetKeyDown(commands.gp_change_camera))
        {
            main_camera.enabled = !main_camera.enabled;
            main_controller.GetComponent<Camera_Controller>().enabled = !main_controller.GetComponent<Camera_Controller>().enabled;
            free_camera.enabled = !free_camera.enabled;
            free_controller.GetComponent<RotationSphere>().enabled = !free_controller.GetComponent<RotationSphere>().enabled;
            Reset_Transform();
        }
    }

    void Reset_Transform()
    {
        if (free_camera.enabled)
        {
            Quaternion new_rot = main_controller.transform.rotation;
            free_controller.transform.rotation = new_rot;
        }
    }
}
