﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Change_Robot_Talk : MonoBehaviour {

    public Player_Commands commands;
    public Robot_Talk talk;
    public Player_Controller player_controller;
    Text robot_text;
    
    public enum Sentences { PRIMA = 0, SECONDA, TERZA, QUARTA, QUINTA, SESTA, SETTIMA, OTTAVA, NONA, DECIMA };
    public Sentences starting_sentence;
    Sentences current_sentence;

    public float max_time = 3f;
    float timer;
    bool can_use_timer;

	// Use this for initialization
	void Start () {
        robot_text = GetComponent<Text>();
        
        current_sentence = starting_sentence;

        timer = 0;
	}
	
	// Update is called once per frame
	void Update () {
        Check_Input();
	}

    void Check_Input()
    {
        switch(current_sentence)
        {
            case Sentences.PRIMA:
                robot_text.text = talk.sentenceses[(int)Sentences.PRIMA];
                Change_Sentence_Timer();
                break;

            case Sentences.SECONDA:
                robot_text.text = talk.sentenceses[(int)Sentences.SECONDA];
                if (Input.GetKeyDown(commands.create) || Input.GetKeyDown(commands.gp_create))
                {
                    current_sentence++;
                }
                break;

            case Sentences.TERZA:
                robot_text.text = talk.sentenceses[(int)Sentences.TERZA];
                if (Input.GetKeyDown(commands.select) || Input.GetKeyDown(commands.gp_select))
                {
                    current_sentence++;
                }
                break;

            case Sentences.QUARTA:
                robot_text.text = talk.sentenceses[(int)Sentences.QUARTA];
                if (player_controller.is_selected)
                {
                    if (Input.GetKeyDown(commands.gp_destroy) || Input.GetKeyDown(commands.destroy))
                    {
                        current_sentence++;
                    }
                }
                
                break;

            case Sentences.QUINTA:
                robot_text.text = talk.sentenceses[(int)Sentences.QUINTA];
                if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetAxisRaw("Right Trigger") >= 1)
                {
                    current_sentence++;
                }
                break;

            case Sentences.SESTA:
                robot_text.text = talk.sentenceses[(int)Sentences.SESTA];
                if (!player_controller.is_selected)
                {
                    if (Input.GetKeyDown(commands.move_up) || Input.GetAxisRaw("DPad Y") >= 1)
                    {
                        robot_text.enabled = false;
                    }
                }
                break;

            case Sentences.SETTIMA:
                robot_text.text = talk.sentenceses[(int)Sentences.SETTIMA];
                Change_Sentence_Timer();
                break;

            case Sentences.OTTAVA:
                robot_text.text = talk.sentenceses[(int)Sentences.OTTAVA];
                if (player_controller.is_selected)
                {
                    if (Input.GetKeyDown(commands.rotate_up) || Input.GetAxisRaw("DPad Y") >= 1)
                    {
                        current_sentence++;
                    }
                }
                break;

            case Sentences.NONA:
                robot_text.text = talk.sentenceses[(int)Sentences.NONA];
                if (Input.GetKeyDown(commands.change_camera) || Input.GetKeyDown(commands.gp_change_camera))
                {
                    current_sentence++;
                }
                break;

            case Sentences.DECIMA:
                robot_text.text = talk.sentenceses[(int)Sentences.DECIMA];
                Change_Sentence_Timer_2();
                break;
        }
    }

    void Change_Sentence_Timer()
    {
        timer += Time.deltaTime;

        if (timer >= max_time)
        {
            current_sentence++;
        }
    }

    void Change_Sentence_Timer_2()
    {
        timer += Time.deltaTime;

        if (timer >= max_time)
        {
            robot_text.enabled = false;
        }
    }
}
