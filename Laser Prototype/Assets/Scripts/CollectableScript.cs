﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableScript : MonoBehaviour {

    MeshRenderer mesh;
    bool touched;

	// Use this for initialization
	void Start () {
        mesh = GetComponent<MeshRenderer>();
        touched = false;
	}
	
	// Update is called once per frame
	void Update () {
        mesh.material.color = new Color(0, 0, 0);
        //touched = false;
    }

    public void SetTrue()
    {
        touched = true;
        mesh.material.color = new Color(1, 1, 1);
        GameObject.FindGameObjectWithTag("GameController").GetComponent<Game_Control>().ControlloVittoria();
    }
    public void SetFalse()
    {
        touched = false;
        mesh.material.color = new Color(0, 0, 0);
    }

    public bool GetTouched()
    {
        return touched;
    }
}
