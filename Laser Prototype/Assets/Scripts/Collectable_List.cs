﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Collectable_List : ScriptableObject {

    public List<GameObject> hitted_collectables;

    private void OnEnable()
    {
        hitted_collectables = new List<GameObject>();
    }
}
