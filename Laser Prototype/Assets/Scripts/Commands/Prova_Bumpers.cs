﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prova_Bumpers : MonoBehaviour {

    public Player_Commands commands;
    public Renderer Left, Right;

    // Use this for initialization
    void Start()
    {
        Left.enabled = false; Right.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(commands.Left_Bumper))
        {
            Left.enabled = true;
        }
        else
        {
            Left.enabled = false;
        }

        if (Input.GetKey(commands.Right_Bumper))
        {
            Right.enabled = true;
        }
        else
        {
            Right.enabled = false;
        }

        
    }
}
