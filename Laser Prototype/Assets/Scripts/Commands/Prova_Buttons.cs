﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prova_Buttons : MonoBehaviour {

    public Player_Commands commands;
    public Renderer A, B, X, Y;

	// Use this for initialization
	void Start () {
        A.enabled = false; B.enabled = false; X.enabled = false; Y.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        
        if (Input.GetKey(commands.A_Button))
        {
            A.enabled = true;
        }
        else
        {
            A.enabled = false;
        }

        if (Input.GetKey(commands.B_Button))
        {
            B.enabled = true;
        }
        else
        {
            B.enabled = false;
        }

        if (Input.GetKey(commands.X_Button))
        {
            X.enabled = true;
        }
        else
        {
            X.enabled = false;
        }

        if (Input.GetKey(commands.Y_Button))
        {
            Y.enabled = true;
        }
        else
        {
            Y.enabled = false;
        }
    }
}
