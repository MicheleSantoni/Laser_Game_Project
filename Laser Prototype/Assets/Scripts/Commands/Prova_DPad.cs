﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prova_DPad : MonoBehaviour {

    public Renderer up, down, right, left;
    public float dead = 0.1f;
	// Use this for initialization
	void Start () {
        up.enabled = false; down.enabled = false; right.enabled = false; left.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        float x_input = Input.GetAxis("DPad X");
        float y_input = Input.GetAxis("DPad Y");
        print(y_input);
        if (up && down)
        {
            if (y_input > dead)
            {
                up.enabled = true;
                down.enabled = false;
            }
            else if (y_input < -dead)
            {
                down.enabled = true;
                up.enabled = false;
            }
            else
            {
                up.enabled = false;
                down.enabled = false;
            }
        }

        if (right && left)
        {
            if (x_input > dead)
            {
                right.enabled = true;
                left.enabled = false;
            }
            else if (x_input < -dead)
            {
                left.enabled = true;
                right.enabled = false;
            }
            else
            {
                right.enabled = false;
                left.enabled = false;
            }
        }
    }
}
