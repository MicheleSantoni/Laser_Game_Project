﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prova_RT : MonoBehaviour {

    public Renderer renderer;

    // Use this for initialization
    void Start()
    {
        renderer.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        float input_value = Input.GetAxis("Right Trigger");
        //print(input_value);
        if (renderer)
        {
            if (input_value > 0.8)
            {
                renderer.enabled = true;
            }
            else
            {
                renderer.enabled = false;
            }
        }
    }
}
