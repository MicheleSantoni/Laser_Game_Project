﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_Hitted_Collectables : MonoBehaviour {

    public Material collectable_on;
    public Material collectable_off;
    List<GameObject> collectables;

    private void OnEnable()
    {
        //print("alive");
        collectables = new List<GameObject>();
    }

    private void OnDisable()
    {
        //print("dead");
        foreach(GameObject collectable in collectables)
        {
            collectable.GetComponent<Collectable_State>().is_hitted = false;
            collectable.GetComponent<MeshRenderer>().material = collectable_off;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Collectable")
        {
            //print("collectable found");
            other.GetComponent<Collectable_State>().is_hitted = true;
            other.GetComponent<MeshRenderer>().material = collectable_on;
            collectables.Add(other.gameObject);
        }


    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Collectable")
        {
            //print("collectable disappear");
            other.GetComponent<Collectable_State>().is_hitted = false;
            other.GetComponent<MeshRenderer>().material = collectable_off;
            collectables.Remove(other.gameObject);
        }
    }
}
