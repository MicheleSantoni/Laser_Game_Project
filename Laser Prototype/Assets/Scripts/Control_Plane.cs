﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_Plane : MonoBehaviour {

    public GameObject pianogioco;
    public LevelCreate level;
    public GameObject game_manager;
    LevelCreate.LevelObject levelObject;

    GameObject def_instance;
    Change_Action.Actions actual_action;
    //public GameObject game_manager;

    private void Awake()
    {
        level.griglia = new GameObject[level.size_grid_lun, level.size_grid_alt, level.size_grid_prof];
        level.collectables = new List<GameObject>();
        level.permanent_objects = new Dictionary<int, GameObject>();
        CreatePianoGame();
        if (level.ListaObject.Count > 0)
        {
            PosObject(level.ListaObject);
        }
    }

    void CreatePianoGame()
    {
        for (int x = 0; x < level.size_grid_lun; x++)
        {
            for (int y = 0; y < level.size_grid_alt; y++)
            {
                for (int z = 0; z < level.size_grid_prof; z++)
                {
                    Vector3 newPosition = new Vector3(x, y, z);
                    def_instance = Instantiate(pianogioco);
                    def_instance.transform.parent = gameObject.transform;
                    def_instance.transform.localPosition = newPosition;
                    level.griglia[x, y, z] = def_instance;
                }
            }
        }
    }


    void PosObject(List<LevelCreate.LevelObject> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            list[i].id = i;
            GameObject g = Instantiate(list[i].GameType);
            g.transform.parent = level.griglia[(int)list[i].PosGame.x, (int)list[i].PosGame.y, (int)list[i].PosGame.z].transform;
            g.transform.localPosition = list[i].GameType.transform.position;
            level.permanent_objects.Add(list[i].id, g);
            GameObject a = null;
            level.permanent_objects.TryGetValue(i, out a);
            print(a);
            if (g.tag == "Collectable")
            {
                print(g);
                level.collectables.Add(g);
            }
        }

        if (game_manager && level.permanent_objects.Count > 0)
        {
            StartCoroutine(game_manager.GetComponent<LookScriptable>().ControlScriptable());
        }
    }
}
