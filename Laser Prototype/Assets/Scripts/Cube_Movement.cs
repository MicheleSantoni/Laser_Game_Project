﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube_Movement : MonoBehaviour {

    public float deg_rot = 0f;
    public float rot_speed = 5f;
    bool is_rotating = false;
    Quaternion start_rotation;
    Quaternion end_rotation;

    private void Start()
    {
        this.enabled = false;
    }

    private void Update()
    {
        Rotate_Cube();
    }

    public void Rotate_Cube()
    {
        //print("Rotate");
        if (!is_rotating)
        {

            if (Input.GetKeyDown(KeyCode.A) || Input.GetMouseButtonDown(1))
            {
                Positive_Rotation();
                //print("start " + start_rotation);
                //print("end " + end_rotation);
                is_rotating = true;
            }
        }
        else
        {
            //print("rotation " + transform.rotation.eulerAngles.y);
            //print("end " + end_rotation.eulerAngles.y);

            transform.rotation = Quaternion.RotateTowards(transform.rotation, end_rotation, rot_speed);

            if (end_rotation.eulerAngles.y > start_rotation.eulerAngles.y)
            {
                if (transform.eulerAngles.y >= end_rotation.eulerAngles.y)
                {
                    //print("rotated");
                    is_rotating = false;
                }
            }
            else
            {
                if (transform.eulerAngles.y <= end_rotation.eulerAngles.y)
                {
                    //print("rotated");
                    is_rotating = false;
                }
            }
        }
    }

    void Positive_Rotation()
    {
        start_rotation = transform.rotation;
        end_rotation = Quaternion.Euler(start_rotation.eulerAngles.x, start_rotation.eulerAngles.y + deg_rot, start_rotation.eulerAngles.z);
    }
}
