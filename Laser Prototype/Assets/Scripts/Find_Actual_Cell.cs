﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Find_Actual_Cell : MonoBehaviour {

    Ray ray;
    RaycastHit hit;
    public Grid_Info grid_info;
    public LineRenderer laser;
    public Material hitted_cell_material;

    // Use this for initialization
    void Start () {
        ray = new Ray();
        hit = new RaycastHit();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        ray.origin = transform.position;
        ray.direction = transform.right;

        if (laser.enabled)
        {
            Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);
            if (Physics.Raycast(ray, out hit, float.MaxValue, 1 << 11 | 1 << 8))
            {
                if (grid_info)
                {
                    if (hit.collider == grid_info.actual_cell.GetComponent<Collider>() && grid_info.actual_cell.GetComponent<Collider>())
                    {
                        if (hit.collider.gameObject.GetComponent<MeshRenderer>().material != hitted_cell_material)
                        {
                            //print("actual cell hitted");
                            hit.collider.gameObject.GetComponent<MeshRenderer>().material = hitted_cell_material;
                        }
                        //print("lol");
                    }
                }
            }
            //StartCoroutine(Change_Hitted_Cell_Material());
        }
    }

    IEnumerator Change_Hitted_Cell_Material()
    {

        while (Physics.Raycast(ray, out hit, float.MaxValue, 1 << 11 | 1 << 8))
        {
            //print(hit.collider);
            if (grid_info)
            {
                if (hit.collider == grid_info.actual_cell.GetComponent<Collider>() && grid_info.actual_cell.GetComponent<Collider>())
                {
                    if (hit.collider.gameObject.GetComponent<MeshRenderer>().material != hitted_cell_material)
                    {
                        //print("actual cell hitted");
                        hit.collider.gameObject.GetComponent<MeshRenderer>().material = hitted_cell_material;
                    }
                    //print("lol");
                }
            }
            
            yield return new WaitForSeconds(0.1f);
        }
    }
}
