﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Find_Collectable : MonoBehaviour {

    Ray ray;
    RaycastHit[] hits;
    RaycastHit raycast_hit;
    public LineRenderer laser;
    public Collectable_List collectables;
    public GameObject col_finder;

    Vector3 start_point = new Vector3();
    //Vector3 end_point = new Vector3();
    float end_point;
    float def_length = 100f;

    // Use this for initialization
    void Start () {
        ray = new Ray();
        raycast_hit = new RaycastHit();

        start_point = transform.position;
        if (col_finder)
        {
            col_finder.transform.position = start_point;
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        ray.origin = transform.position;
        ray.direction = transform.right;

        

        if (laser.enabled)
        {
            Debug.DrawRay(ray.origin, ray.direction * 10, Color.green);

            if (Physics.Raycast(ray, out raycast_hit, float.MaxValue, 1 << 8))
            {
                end_point = Vector3.Distance(start_point, raycast_hit.point);
                if (col_finder)
                {

                    col_finder.transform.localScale = new Vector3(1f, end_point * 10, 1f);
                }
            }
            else
            {
                if (col_finder)
                {
                    col_finder.transform.localScale = new Vector3(1f, def_length, 1f);
                }
            }
                
        }
        else
        {
            if (col_finder)
            {
                col_finder.transform.localScale = new Vector3(1f, 1f, 1f);
            }
        }
    }
}
