﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Grid_Info : ScriptableObject {

    public GameObject actual_cell;

    private void OnDisable()
    {
        actual_cell = null;
    }
}
