﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Laser_1 : MonoBehaviour {

    public float time_between_lasers = 0.15f;
    public float default_range = 100f;

    float timer;
    Ray first_ray;
    RaycastHit first_hit;
    List<RaycastHit> hit_list = new List<RaycastHit>();
    List<Ray> ray_list = new List<Ray>();
    Ray temp_ray = new Ray();
    RaycastHit temp_hit = new RaycastHit();
    Vector3 new_origin, new_direction;

    int shootable_mask;
    float hit_number;

    LineRenderer laser_line;

    public float effect_display_time = 0.2f;
    public bool is_shooting = false;

	// Use this for initialization
	void Start () {
        hit_list.Add(first_hit);
        ray_list.Add(first_ray);

        shootable_mask = LayerMask.GetMask("Shootable");
        laser_line = GetComponent<LineRenderer>();
        laser_line.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        //timer += Time.deltaTime;

        if (is_shooting)
        {
            StartCoroutine(Shoot());
        }
        else
        {
            laser_line.enabled = false;
        }
	}

    IEnumerator Shoot()
    {
        laser_line.enabled = true;

        Ray startRay = new Ray(transform.position, transform.forward);
        laser_line.SetPosition(0, startRay.origin);
        
        //Debug.DrawRay(startRay.origin, startRay.direction*10);
        int i = 1;
        RaycastHit hitInfo;
        GameObject previusObject = null;

        while(Physics.Raycast(startRay,out hitInfo, default_range, shootable_mask))
        {
            print(hitInfo.collider);
            laser_line.SetPosition(i, hitInfo.point);
            Vector3 newDir = Vector3.Reflect(hitInfo.point - startRay.origin, hitInfo.normal);
            Ray newRay = new Ray(hitInfo.point, newDir);
            laser_line.SetPosition(i + 1, newDir * default_range);

            //Debug.DrawRay(newRay.origin,newRay.direction*10);

            startRay = newRay;
            
            if (previusObject == hitInfo.collider.gameObject)
            {
                break;
            }
            else
            {
                i++;
                laser_line.positionCount = i + 2;
                previusObject = hitInfo.collider.gameObject;
            }
            laser_line.SetPosition(laser_line.positionCount - 1, hitInfo.point);

            yield return null;
        }
    }
}
