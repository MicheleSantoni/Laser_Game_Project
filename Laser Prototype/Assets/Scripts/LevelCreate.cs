﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MyScriptableObject", menuName = "Level", order = 0)]
public  class LevelCreate : ScriptableObject {
  
   
    //public int sizeGrid;
    public int size_grid_alt, size_grid_lun, size_grid_prof;
    [HideInInspector]
    public Vector3 posGrid;
    public List<LevelObject> ListaObject;
    public Dictionary<int, GameObject> permanent_objects;
    public GameObject[,,] griglia;
    [HideInInspector]
    public List<GameObject> collectables; 

    [System.Serializable]
    public class LevelObject
    {
        [HideInInspector]
        public int id;
        public GameObject GameType;
        public Vector3 PosGame;
        //public Quaternion RotGame;
        public Vector3 RotGame;
    }
    
}
