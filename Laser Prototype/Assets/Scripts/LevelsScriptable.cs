﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "LevelScriptable",menuName ="LevelsManager")]
public class LevelsScriptable : ScriptableObject {

    public string[] levels;

}
