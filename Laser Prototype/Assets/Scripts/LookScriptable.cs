﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookScriptable : MonoBehaviour {

    public LevelCreate level;
    GameObject g;
    bool victory;
    public Canvas canvas_victory;
    Coroutine check;


    private void Start()
    {
        victory = false;
        canvas_victory.enabled = false;
        
    }

    private void FixedUpdate()
    {

        if (!victory)
        {
            StartCoroutine(Check_Victory());
        }
        
        if (level.permanent_objects.Count > 0)
        {
            StartCoroutine(ControlScriptable());
        }
    }

    public IEnumerator ControlScriptable()
    {
        g = null;
        //print("look scriptable");
        //print("permanent objects count " + level.permanent_objects.Keys.Count);
        //print("collectables count " + level.collectables.Count);
        foreach (int i in level.permanent_objects.Keys)
        {
            //print(i);
            foreach (LevelCreate.LevelObject lv in level.ListaObject)
            {
                if (i == lv.id)
                {
                    level.permanent_objects.TryGetValue(i, out g);
                    //print(g + " " + i);
                    g.transform.parent = level.griglia[(int)lv.PosGame.x, (int)lv.PosGame.y, (int)lv.PosGame.z].transform;
                    //lv.PosGame = new Vector3(lv.PosGame.x, lv.PosGame.y, lv.PosGame.z);
                    g.transform.localPosition = lv.GameType.transform.position;
                    g.transform.eulerAngles = new Vector3(lv.RotGame.x, lv.RotGame.y, lv.RotGame.z);
                }
                yield return null;
            }

        }
        
    }

    IEnumerator Check_Victory()
    {
        //print("check victory");

        for (int i = 0; i < level.collectables.Count; i++)
        {
            if (!level.collectables[i].GetComponent<Collectable_State>().is_hitted)
            {
                //print(i);
                //i = level.collectables.Count + 1;
                //print("break it");
                break;
            }

            if (i == level.collectables.Count - 1)
            {
                print("victory!!");
                victory = true;
                canvas_victory.enabled = true;
                // print("victory");
            }

            yield return null;
        }
    }

}
