﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MyCommands", menuName = "Commands", order = 0)]
public class Player_Commands : ScriptableObject {
    [Header("Comandi Di Riferimento (NON MODIFICARE)")]

    public KeyCode A_Button = KeyCode.Joystick1Button0;
    public KeyCode B_Button = KeyCode.Joystick1Button1, 
        X_Button = KeyCode.Joystick1Button2, 
        Y_Button = KeyCode.Joystick1Button3, 
        Right_Bumper = KeyCode.Joystick1Button5, 
        Left_Bumper = KeyCode.Joystick1Button4;

    [Header("Comandi GamePad")]

    public KeyCode gp_create;
    public KeyCode gp_select, gp_deselect, gp_destroy, gp_forward, gp_back, gp_change_camera;

    [Header("Comandi Keyboard")]

    public KeyCode move_up;
    public KeyCode move_down, move_forward, move_back, move_left, move_right, create, select, deselect, destroy, rotate_up, rotate_down, rotate_left, rotate_right, change_camera;
}
