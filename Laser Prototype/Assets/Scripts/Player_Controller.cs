﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Controller : MonoBehaviour {

    public LevelCreate level;
    public Player_Commands commands;
    public Grid_Info grid_info;
    public Sprite[] cube_number_sprites;
    public Image UI_quantity_image;

    public GameObject cube;
    public GameObject camera_handle;

    GameObject actual_cell;
    GameObject previous_cell;
    GameObject selected_cube;

    public Material mat_non_target_cell;
    public Material mat_target_cell;
    public Material mat_selected_cell;
    public Material mat_surrounding_cell;

    public float cube_rotation_vel, input_delay_time = 0.1f;
    float input_timer;
    public int max_cubes = 2;
    int n_cubes;
    int prev_x, prev_y, prev_z;

    [HideInInspector]
    public bool is_selected;
    bool is_rotating, is_max_cubes, can_move;

    enum Camera_Directions { FACE1, FACE2, FACE3, FACE4, FACE5, FACE6};
    Camera_Directions direction, prev_direction;
    
    // Use this for initialization
    void Start ()
    {
        is_selected = false;
        is_rotating = false;
        can_move = true;
        input_timer = 0;
        
        // controllo se è stato collegato lo scriptable object del livello
        if (level)
        {
            // assegno delle coordinate iniziali alla cella che viene utilizzata come target
            prev_x = level.size_grid_lun / 2;
            prev_y = level.size_grid_alt / 2;
            prev_z = 0;
            //print(prev_x + "   " + prev_y);
            actual_cell = level.griglia[prev_x, prev_y, prev_z].gameObject;
            actual_cell.layer = 11;
            grid_info.actual_cell = actual_cell;
            Change_Plane_Material();
            Change_Actual_Cell_Material();
        }
        // se non è collegato nulla mando in console un messaggio di errore
        else
        {
            Debug.LogError("LevelCreate asset mancante");
        }

        n_cubes = 0;
        //print("is enabled");
        Change_Cube_Number();
    }

    //_____________________________________________________________________________________________________
	
	// Update is called once per frame
	void Update () {
        // richiamo la funzione che controlla l'input del giocatore
        Camera_Direction();
        Input_Player();
	}

    //_____________________________________________________________________________________________________

    void Input_Player()
    {
        // controllo se il comando per selezionare è uguale al comando per deselezionare
        
        if(commands.select == commands.deselect || commands.gp_select == commands.gp_deselect)
        {
            // se i due comandi sono uguali allora richiamo la funzione per distinguere le due situazioni (se il giocatore vuole selezionare oppure deselezionare)
            Select_Deselect_Cube();
        }
        else
        {
            // Controllo se il giocatore ha premuto il tasto per deselezionare un Cubo
            if (Input.GetKeyDown(commands.deselect) || Input.GetKeyDown(commands.gp_deselect))
            {
                if (is_selected)
                {
                    // se la cella target (actual_cell) ha un cubo come figlio richiamo la funzione per la selezione
                    if (actual_cell.transform.GetChild(0).tag == "Cube")
                    {
                        Deselect_Cube();
                    }
                    //if (grid_info.actual_cell.transform.GetChild(0).tag == "Cube")
                    //{
                    //    Deselect_Cube();
                    //}
                }
            }

            // Controllo se il giocatore ha premuto il tasto per selezionare un Cubo
            if (Input.GetKeyDown(commands.select) || Input.GetKeyDown(commands.gp_select))
            {
                if (!is_selected)
                {
                    // controllo se la cella target della griglia abbia figli
                    if (actual_cell.transform.childCount > 0)
                    {
                        // se la cella target (actual_cell) ha un cubo come figlio richiamo la funzione per la selezione
                        if (actual_cell.transform.GetChild(0).tag == "Cube")
                        {
                            //print(actual_cell.transform.GetChild(0).name);

                            Select_Cube();
                        }
                    }

                    //if (grid_info.actual_cell.transform.childCount > 0)
                    //{                        
                    //    if (grid_info.actual_cell.transform.GetChild(0).tag == "Cube")
                    //    {
                    //        Select_Cube();
                    //    }
                    //}
                }
            }
        }

        
        if (commands.create == commands.destroy || commands.gp_create == commands.gp_destroy)
        {
            Create_Destroy_Cube();
        }
        else
        {
            
            // Controllo se il giocatore ha premutoil tasto per Inserire un nuovo cubo
            if (Input.GetKeyDown(commands.create) || Input.GetKeyDown(commands.gp_create))
            {
                Check_Cube_Quantity();
                if (!is_max_cubes)
                {
                    // controllo se la cella target della griglia non abbia figli
                    if (actual_cell.transform.childCount == 0)
                    {
                        // se non ne ha richiamo la funzione che aggiunge un cubo
                        Add_Cube();
                    }
                }
            }

            // controllo se il giocatore ha premuto il tasto per cancellare il cubo selezionato
            if (Input.GetKeyDown(commands.destroy) || Input.GetKeyDown(commands.gp_destroy))
            {
                if (is_selected)
                {
                    // se la cella target (actual_cell) ha un cubo come figlio richiamo la funzione per la cancellazione
                    if (actual_cell.transform.GetChild(0).tag == "Cube")
                    {
                        //print(actual_cell.transform.GetChild(0).name);
                        Erase_Cube();
                        Check_Cube_Quantity();
                    }

                    //if (grid_info.actual_cell.transform.GetChild(0).tag == "Cube")
                    //{
                    //    Erase_Cube();
                    //}
                }
            }
        }
        
        

        

        // controllo che un cubo sia stato selezionato
        if (is_selected)
        {
            // se si richiamo la funzione per ruotare il cubo che è all'interno della cella target(actual_cell)
            //Change_Cube_Rotation();
            Rotate_Selected_Cube();
        }
        else
        {
            // se no do la possibilità al giocatore di cambiare cella
            Cambia_Cella_Attuale();
            //Change_Actual_Cell();
        }
        
    }

    //_____________________________________________________________________________________________________

    void Check_Cube_Quantity()
    {
        if (n_cubes >= max_cubes)
        {
            is_max_cubes = true;
        }
        else
        {
            is_max_cubes = false;
        }
    }

    //_____________________________________________________________________________________________________

    void Camera_Direction()
    {
        if (camera_handle)
        {

            if (camera_handle.transform.forward == new Vector3(0, 0, 1))
            {
                direction = Camera_Directions.FACE1;
                
            }
            else if (camera_handle.transform.forward == new Vector3(1, 0, 0))
            {
                direction = Camera_Directions.FACE2;
            }
            else if (camera_handle.transform.forward == new Vector3(0, 0, -1))
            {
                direction = Camera_Directions.FACE3;
            }
            else if (camera_handle.transform.forward == new Vector3(-1, 0, 0))
            {
                direction = Camera_Directions.FACE4;
            }
            else if (camera_handle.transform.forward == new Vector3(0, 1, 0))
            {
                direction = Camera_Directions.FACE5;
            }
            else if (camera_handle.transform.forward == new Vector3(0, -1, 0))
            {
                direction = Camera_Directions.FACE6;
            }

            if (direction != prev_direction)
            {
                print("difference");
                Reset_Plane_Material();
                Change_Plane_Material();
                if (!is_selected)
                {
                    Change_Actual_Cell_Material();
                }
                else
                {
                    Change_Selected_Cell_Material();
                }
            }
            prev_direction = direction;
            //print(direction + " " + camera_handle.transform.up);
            //print(direction);
        }
    }

    //_____________________________________________________________________________________________________

    IEnumerator Input_Delay()
    {
        while (input_timer < input_delay_time)
        {
            input_timer += Time.deltaTime;
            can_move = false;
            yield return null;
        }
        can_move = true;
        input_timer = 0;
    }

    //_____________________________________________________________________________________________________

    void Cambia_Cella_Attuale()
    {
        if (can_move)
        {
            if (Input.GetKeyDown(commands.move_forward) || Input.GetKeyDown(commands.gp_forward))
            {
                switch (direction)
                {
                    case Camera_Directions.FACE1:
                        prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                        //print("cambio cella");
                        Update_Actual_Cell();
                        break;

                    case Camera_Directions.FACE2:
                        prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                        Update_Actual_Cell();
                        break;

                    case Camera_Directions.FACE3:
                        prev_z = Decrease_Coordinate(prev_z);
                        Update_Actual_Cell();
                        break;

                    case Camera_Directions.FACE4:
                        prev_x = Decrease_Coordinate(prev_x);
                        Update_Actual_Cell();
                        break;

                    case Camera_Directions.FACE5:
                        prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                        Update_Actual_Cell();
                        break;

                    case Camera_Directions.FACE6:
                        prev_y = Decrease_Coordinate(prev_y);
                        Update_Actual_Cell();
                        break;

                    default:
                        break;
                }
            }
            else if (Input.GetKeyDown(commands.move_back) || Input.GetKeyDown(commands.gp_back))
            {
                switch (direction)
                {
                    case Camera_Directions.FACE1:
                        prev_z = Decrease_Coordinate(prev_z);
                        Update_Actual_Cell();
                        break;

                    case Camera_Directions.FACE2:
                        prev_x = Decrease_Coordinate(prev_x);
                        Update_Actual_Cell();
                        break;

                    case Camera_Directions.FACE3:
                        prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                        Update_Actual_Cell();
                        break;

                    case Camera_Directions.FACE4:
                        prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                        Update_Actual_Cell();
                        break;

                    case Camera_Directions.FACE5:
                        prev_y = Decrease_Coordinate(prev_y);
                        Update_Actual_Cell();
                        break;

                    case Camera_Directions.FACE6:
                        prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                        Update_Actual_Cell();
                        break;

                    default:
                        break;
                }
            }
            else if (Input.GetKey(commands.move_right) || Input.GetAxisRaw("DPad X") >= 1)
            {
                switch (direction)
                {
                    case Camera_Directions.FACE1:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            prev_x = Decrease_Coordinate(prev_x);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            prev_y = Decrease_Coordinate(prev_y);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE2:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            prev_z = Decrease_Coordinate(prev_z);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            prev_y = Decrease_Coordinate(prev_y);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            prev_y = Increase_Coordinate(prev_y,level.size_grid_alt);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE3:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            prev_x = Decrease_Coordinate(prev_x);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            prev_y = Decrease_Coordinate(prev_y);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE4:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            prev_z = Decrease_Coordinate(prev_z);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            prev_y = Decrease_Coordinate(prev_y);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE5:
                        if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            prev_x = Decrease_Coordinate(prev_x);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            prev_z = Increase_Coordinate(prev_z,level.size_grid_prof);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            prev_z = Decrease_Coordinate(prev_z);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE6:
                        if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            prev_x = Decrease_Coordinate(prev_x);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            prev_z = Decrease_Coordinate(prev_z);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                            Update_Actual_Cell();
                        }
                        break;

                    default:
                        break;
                }
            }
            else if (Input.GetKey(commands.move_left) || Input.GetAxisRaw("DPad X") <= -1)
            {
                switch (direction)
                {
                    case Camera_Directions.FACE1:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            prev_x = Decrease_Coordinate(prev_x);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            prev_y = Decrease_Coordinate(prev_y);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE2:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            prev_z = Decrease_Coordinate(prev_z);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            prev_y = Decrease_Coordinate(prev_y);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE3:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            prev_x = Decrease_Coordinate(prev_x);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            prev_y = Decrease_Coordinate(prev_y);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE4:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            prev_z = Decrease_Coordinate(prev_z);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            prev_y = Decrease_Coordinate(prev_y);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE5:
                        if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            prev_x = Decrease_Coordinate(prev_x);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            prev_z = Decrease_Coordinate(prev_z);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE6:
                        if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            prev_x = Decrease_Coordinate(prev_x);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            prev_z = Decrease_Coordinate(prev_z);
                            Update_Actual_Cell();
                        }
                        break;

                    default:
                        break;
                }
            }
            else if (Input.GetKey(commands.move_up) || Input.GetAxisRaw("DPad Y") >= 1)
            {
                switch (direction)
                {
                    case Camera_Directions.FACE1:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            prev_y = Decrease_Coordinate(prev_y);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            prev_x = Decrease_Coordinate(prev_x);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE2:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            prev_y = Decrease_Coordinate(prev_y);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            prev_z = Decrease_Coordinate(prev_z);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE3:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            prev_y = Decrease_Coordinate(prev_y);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            prev_x = Decrease_Coordinate(prev_x);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE4:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            prev_y = Decrease_Coordinate(prev_y);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            prev_z = Decrease_Coordinate(prev_z);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE5:
                        if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            prev_z = Decrease_Coordinate(prev_z);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            prev_x = Decrease_Coordinate(prev_x);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE6:
                        if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            prev_z = Decrease_Coordinate(prev_z);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            prev_x = Decrease_Coordinate(prev_x);
                            Update_Actual_Cell();
                        }
                        break;

                    default:
                        break;
                }
            }
            else if (Input.GetKey(commands.move_down) || Input.GetAxisRaw("DPad Y") <= -1)
            {
                switch (direction)
                {
                    case Camera_Directions.FACE1:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            prev_y = Decrease_Coordinate(prev_y);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            prev_x = Decrease_Coordinate(prev_x);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE2:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            prev_y = Decrease_Coordinate(prev_y);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            prev_z = Decrease_Coordinate(prev_z);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE3:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            prev_y = Decrease_Coordinate(prev_y);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            prev_x = Decrease_Coordinate(prev_x);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE4:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            prev_y = Decrease_Coordinate(prev_y);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            prev_y = Increase_Coordinate(prev_y, level.size_grid_alt);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            prev_z = Decrease_Coordinate(prev_z);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE5:
                        if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            prev_z = Decrease_Coordinate(prev_z);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            prev_x = Decrease_Coordinate(prev_x);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                            Update_Actual_Cell();
                        }
                        break;

                    case Camera_Directions.FACE6:
                        if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            prev_z = Decrease_Coordinate(prev_z);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            prev_z = Increase_Coordinate(prev_z, level.size_grid_prof);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            prev_x = Decrease_Coordinate(prev_x);
                            Update_Actual_Cell();
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            prev_x = Increase_Coordinate(prev_x, level.size_grid_lun);
                            Update_Actual_Cell();
                        }
                        break;

                    default:
                        break;
                }
            }
        }
    }

    //_____________________________________________________________________________________________________

    int Increase_Coordinate(int coordinate, int max_value)
    {
        if (!Check_Coordinate_Max(coordinate, max_value))
        {
            coordinate++;
        }
        return coordinate;
    }

    //_____________________________________________________________________________________________________

    int Decrease_Coordinate(int coordinate)
    {
        if (!Check_Coordinate_Min(coordinate))
        {
            coordinate--;
        }
        return coordinate;
    }

    //_____________________________________________________________________________________________________

    

    void Update_Actual_Cell()
    {
        previous_cell = actual_cell;
        previous_cell.layer = 9;
        actual_cell = level.griglia[prev_x, prev_y, prev_z];
        actual_cell.layer = 11;
        grid_info.actual_cell = actual_cell;
        StartCoroutine(Input_Delay());
        //Change_Prev_Cell_Material();
        Reset_Plane_Material();
        Change_Plane_Material();
        Change_Actual_Cell_Material();
        
    }

    //_____________________________________________________________________________________________________

    void Create_Destroy_Cube()
    { 
        if (Input.GetKeyDown(commands.create) || Input.GetKeyDown(commands.gp_create))
        {
            if (!is_selected)
            {
                Check_Cube_Quantity();
                if (!is_max_cubes)
                {
                    // controllo se la cella target della griglia non abbia figli
                    if (actual_cell.transform.childCount == 0)
                    {
                        // se non ne ha richiamo la funzione che aggiunge un cubo
                        Add_Cube();
                    }
                }
            }
            else
            {
                // se la cella target (actual_cell) ha un cubo come figlio richiamo la funzione per la cancellazione
                if (actual_cell.transform.GetChild(0).tag == "Cube")
                {
                    //print(actual_cell.transform.GetChild(0).name);
                    Erase_Cube();
                    Check_Cube_Quantity();
                }
            }
        }
    }

    //_____________________________________________________________________________________________________

    void Select_Deselect_Cube()
    {
        if (Input.GetKeyDown(commands.select) || Input.GetKeyDown(commands.gp_select))
        {
            if (is_selected)
            {
                // se la cella target (actual_cell) ha un cubo come figlio richiamo la funzione per la selezione
                if (actual_cell.transform.GetChild(0).tag == "Cube")
                {
                    Deselect_Cube();
                }

                //if (grid_info.actual_cell.transform.GetChild(0).tag == "Cube")
                //{
                //    Deselect_Cube();
                //}
            }
            else if (!is_selected)
            {
                // controllo se la cella target della griglia abbia figli
                if (actual_cell.transform.childCount > 0)
                {
                    // se la cella target (actual_cell) ha un cubo come figlio richiamo la funzione per la selezione
                    if (actual_cell.transform.GetChild(0).tag == "Cube")
                    {
                        //print(actual_cell.transform.GetChild(0).name);

                        Select_Cube();
                    }
                }

                //if (grid_info.actual_cell.transform.childCount > 0)
                //{
                //    if (grid_info.actual_cell.transform.GetChild(0).tag == "Cube")
                //    {
                //        //print(actual_cell.transform.GetChild(0).name);

                //        Select_Cube();
                //    }
                //}
            }
        }
    }

    //_____________________________________________________________________________________________________

    void Change_Cube_Number()
    {
        print("n_cubes " + n_cubes + " max cubes " + max_cubes);
        if (UI_quantity_image)
        {
            UI_quantity_image.overrideSprite = cube_number_sprites[max_cubes - n_cubes];
        }
        else
        {
            Debug.LogError("UI_quantity_image asset mancante!");
        }
    }

    //_____________________________________________________________________________________________________

    void Add_Cube()
    {
        // instanzio il cubo nel mondo di gioco
        GameObject instance;
        instance = Instantiate(cube);
        // specifico che è figlio della cella target ("actual_cell")
        instance.transform.parent = actual_cell.transform;
        //instance.transform.parent = grid_info.actual_cell.transform;
        // e imposto la sua position al centro della cella
        instance.transform.localPosition = cube.transform.position;
        n_cubes++;
        Change_Cube_Number();
        Select_Cube();
    }

    //_____________________________________________________________________________________________________

    void Select_Cube()
    {
        is_selected = true;

        actual_cell.GetComponent<Collider>().enabled = false;
        Change_Selected_Cell_Material();
    }

    //_____________________________________________________________________________________________________

    void Erase_Cube()
    {
        Destroy(actual_cell.transform.GetChild(0).gameObject);
        n_cubes--;
        Change_Cube_Number();
        //Destroy(grid_info.actual_cell.transform.GetChild(0).gameObject);
        Deselect_Cube();
    }

    //_____________________________________________________________________________________________________

    void Deselect_Cube()
    {
        is_selected = false;
        //print("deselect");
        actual_cell.GetComponent<Collider>().enabled = true;
        Change_Actual_Cell_Material();
    }

    //_____________________________________________________________________________________________________

    void Rotate_Selected_Cube()
    {     


        // controllo se il cubo selezionato non sia in rotaione
        if (!is_rotating)
        {

            // se viene premuto un tasto per la rotazione allora richiamo la funzione per fa ruotare il cubo selezionato

            // ROTAZIONE IN SU
            if (Input.GetKeyDown(commands.rotate_up) || Input.GetAxisRaw("DPad Y") >= 1)
            {
                switch(direction)
                {
                    case Camera_Directions.FACE1:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            StartCoroutine(Rotate_X(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            StartCoroutine(Rotate_X(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            StartCoroutine(Rotate_Y(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            StartCoroutine(Rotate_Y(90f));
                        }
                        break;

                    case Camera_Directions.FACE2:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            StartCoroutine(Rotate_Z(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            StartCoroutine(Rotate_Z(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            StartCoroutine(Rotate_Y(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            StartCoroutine(Rotate_Y(90f));
                        }
                        break;

                    case Camera_Directions.FACE3:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            StartCoroutine(Rotate_X(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            StartCoroutine(Rotate_X(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            StartCoroutine(Rotate_Y(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            StartCoroutine(Rotate_Y(-90f));
                        }
                        break;

                    case Camera_Directions.FACE4:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            StartCoroutine(Rotate_Z(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            StartCoroutine(Rotate_Z(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            StartCoroutine(Rotate_Y(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            StartCoroutine(Rotate_Y(-90f));
                        }
                        break;

                    case Camera_Directions.FACE5:
                        if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            StartCoroutine(Rotate_X(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            StartCoroutine(Rotate_X(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            StartCoroutine(Rotate_Z(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            StartCoroutine(Rotate_Z(-90f));
                        }
                        break;

                    case Camera_Directions.FACE6:
                        if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            StartCoroutine(Rotate_X(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            StartCoroutine(Rotate_X(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            StartCoroutine(Rotate_Z(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            StartCoroutine(Rotate_Z(90f));
                        }
                        break;
                }
            }

            // ROTAZIONE IN GIU'
            else if (Input.GetKeyDown(commands.rotate_down) || Input.GetAxisRaw("DPad Y") <= -1)
            {
                switch(direction)
                {
                    case Camera_Directions.FACE1:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            StartCoroutine(Rotate_X(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            StartCoroutine(Rotate_X(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            StartCoroutine(Rotate_Y(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            StartCoroutine(Rotate_Y(-90f));
                        }
                        break;

                    case Camera_Directions.FACE2:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            StartCoroutine(Rotate_Z(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            StartCoroutine(Rotate_Z(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            StartCoroutine(Rotate_Y(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            StartCoroutine(Rotate_Y(-90f));
                        }
                        break;

                    case Camera_Directions.FACE3:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            StartCoroutine(Rotate_X(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            StartCoroutine(Rotate_X(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            StartCoroutine(Rotate_Y(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            StartCoroutine(Rotate_Y(90f));
                        }
                        break;

                    case Camera_Directions.FACE4:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            StartCoroutine(Rotate_Z(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            StartCoroutine(Rotate_Z(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            StartCoroutine(Rotate_Y(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            StartCoroutine(Rotate_Y(90f));
                        }
                        break;

                    case Camera_Directions.FACE5:
                        if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            StartCoroutine(Rotate_X(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            StartCoroutine(Rotate_X(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            StartCoroutine(Rotate_Z(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            StartCoroutine(Rotate_Z(90f));
                        }
                        break;

                    case Camera_Directions.FACE6:
                        if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            StartCoroutine(Rotate_X(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            StartCoroutine(Rotate_X(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            StartCoroutine(Rotate_Z(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            StartCoroutine(Rotate_Z(-90f));
                        }
                        break;
                }
            }

            // ROTAZIONE A DESTRA
            else if (Input.GetKeyDown(commands.rotate_right) || Input.GetAxisRaw("DPad X") >= 1)
            {
                switch (direction)
                {
                    case Camera_Directions.FACE1:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            StartCoroutine(Rotate_Y(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            StartCoroutine(Rotate_Y(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            StartCoroutine(Rotate_X(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            StartCoroutine(Rotate_X(90f));
                        }
                        break;

                    case Camera_Directions.FACE2:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            StartCoroutine(Rotate_Y(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            StartCoroutine(Rotate_Y(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            StartCoroutine(Rotate_Z(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            StartCoroutine(Rotate_Z(-90f));
                        }
                        break;

                    case Camera_Directions.FACE3:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            StartCoroutine(Rotate_Y(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            StartCoroutine(Rotate_Y(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            StartCoroutine(Rotate_X(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            StartCoroutine(Rotate_X(90f));
                        }
                        break;

                    case Camera_Directions.FACE4:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            StartCoroutine(Rotate_Y(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            StartCoroutine(Rotate_Y(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            StartCoroutine(Rotate_Z(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            StartCoroutine(Rotate_Z(-90f));
                        }
                        break;

                    case Camera_Directions.FACE5:
                        if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            StartCoroutine(Rotate_Z(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            StartCoroutine(Rotate_Z(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            StartCoroutine(Rotate_X(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            StartCoroutine(Rotate_X(90f));
                        }
                        break;

                    case Camera_Directions.FACE6:
                        if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            StartCoroutine(Rotate_Z(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            StartCoroutine(Rotate_Z(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            StartCoroutine(Rotate_X(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            StartCoroutine(Rotate_X(90f));
                        }
                        break;
                }
            }

            // ROTAZIONE A SINISTRA
            else if (Input.GetKeyDown(commands.rotate_left) || Input.GetAxisRaw("DPad X") <= -1)
            {
                switch (direction)
                {
                    case Camera_Directions.FACE1:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            StartCoroutine(Rotate_Y(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            StartCoroutine(Rotate_Y(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            StartCoroutine(Rotate_X(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            StartCoroutine(Rotate_X(-90f));
                        }
                        break;

                    case Camera_Directions.FACE2:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            StartCoroutine(Rotate_Y(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            StartCoroutine(Rotate_Y(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            StartCoroutine(Rotate_Z(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            StartCoroutine(Rotate_Z(90f));
                        }
                        break;

                    case Camera_Directions.FACE3:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            StartCoroutine(Rotate_Y(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            StartCoroutine(Rotate_Y(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            StartCoroutine(Rotate_X(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            StartCoroutine(Rotate_X(-90f));
                        }
                        break;

                    case Camera_Directions.FACE4:
                        if (camera_handle.transform.up == new Vector3(0, 1, 0))
                        {
                            StartCoroutine(Rotate_Y(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, -1, 0))
                        {
                            StartCoroutine(Rotate_Y(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            StartCoroutine(Rotate_Z(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            StartCoroutine(Rotate_Z(90f));
                        }
                        break;

                    case Camera_Directions.FACE5:
                        if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            StartCoroutine(Rotate_Z(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            StartCoroutine(Rotate_Z(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            StartCoroutine(Rotate_X(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            StartCoroutine(Rotate_X(-90f));
                        }
                        break;

                    case Camera_Directions.FACE6:
                        if (camera_handle.transform.up == new Vector3(0, 0, -1))
                        {
                            StartCoroutine(Rotate_Z(-90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(0, 0, 1))
                        {
                            StartCoroutine(Rotate_Z(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(1, 0, 0))
                        {
                            StartCoroutine(Rotate_X(90f));
                        }
                        else if (camera_handle.transform.up == new Vector3(-1, 0, 0))
                        {
                            StartCoroutine(Rotate_X(-90f));
                        }
                        break;
                }
            }
        }
    }

    //_____________________________________________________________________________________________________

    IEnumerator Rotate_X(float angle)
    {
        selected_cube = actual_cell.transform.GetChild(0).gameObject;
        //selected_cube.transform.localRotation = Quaternion.LookRotation(selected_cube.transform.forward, camera_handle.transform.up);

        Vector3 eulerRot = selected_cube.transform.rotation.eulerAngles;
        float startRotation = eulerRot.x;

        float i = 0;
        is_rotating = true;

        while (i < cube_rotation_vel)
        {

            selected_cube.transform.Rotate(angle / cube_rotation_vel, 0, 0, Space.World);
            i++;

            yield return null;
        }
        //selected_cube.transform.TransformDirection(actual_cell.transform.forward + actual_cell.transform.up);

        is_rotating = false;
    }

    //_____________________________________________________________________________________________________

    IEnumerator Rotate_Y(float angle)
    {
        selected_cube = actual_cell.transform.GetChild(0).gameObject;
        //selected_cube.transform.localRotation = Quaternion.LookRotation(selected_cube.transform.forward, camera_handle.transform.up);


        Vector3 eulerRot = selected_cube.transform.rotation.eulerAngles;
        float startRotation = eulerRot.y;

        float i = 0;
        is_rotating = true;

        //selected_cube.transform.localRotation = Quaternion.LookRotation(camera_handle.transform.forward, camera_handle.transform.up);
        while (i < cube_rotation_vel)
        {

            selected_cube.transform.Rotate(0, angle / cube_rotation_vel, 0, Space.World);
            i++;

            yield return null;
        }
        //selected_cube.transform.TransformDirection(actual_cell.transform.forward + actual_cell.transform.up);
        is_rotating = false;

        //selected_cube.transform.localRotation = Quaternion.LookRotation(camera_handle.transform.forward, camera_handle.transform.up);
    }

    //_____________________________________________________________________________________________________

    IEnumerator Rotate_Z(float angle)
    {
        selected_cube = actual_cell.transform.GetChild(0).gameObject;
        //selected_cube.transform.localRotation = Quaternion.LookRotation(selected_cube.transform.forward, camera_handle.transform.up);


        Vector3 eulerRot = selected_cube.transform.rotation.eulerAngles;
        float startRotation = eulerRot.z;

        float i = 0;
        is_rotating = true;

        //selected_cube.transform.localRotation = Quaternion.LookRotation(camera_handle.transform.forward, camera_handle.transform.up);
        while (i < cube_rotation_vel)
        {

            selected_cube.transform.Rotate(0, 0, angle / cube_rotation_vel, Space.World);
            i++;

            yield return null;
        }
        //selected_cube.transform.TransformDirection(actual_cell.transform.forward + actual_cell.transform.up);
        is_rotating = false;

        //selected_cube.transform.localRotation = Quaternion.LookRotation(camera_handle.transform.forward, camera_handle.transform.up);
    }

    //_____________________________________________________________________________________________________

    void Change_Actual_Cell_Material()
    {
        // assegno alla attuale cella target (actual_cell) il material adeguato
        actual_cell.GetComponent<MeshRenderer>().material = mat_target_cell;
    }

    //_____________________________________________________________________________________________________

    void Change_Prev_Cell_Material()
    {
        // assegno alla precedente cella target (previous_cell) il material adeguato
        previous_cell.GetComponent<MeshRenderer>().material = mat_non_target_cell;
    }

    //_____________________________________________________________________________________________________

    void Change_Selected_Cell_Material()
    {
        // assegno alla cella target (previous_cell) selezionata il material adeguato
        actual_cell.GetComponent<MeshRenderer>().material = mat_selected_cell;
    }

    //_____________________________________________________________________________________________________

    public void Change_Plane_Material()
    {

        // il piano rimane perpendicolare alla camera

        // controllo in quale faccia della griglia sono
        if (direction == Camera_Directions.FACE1 || direction == Camera_Directions.FACE3)
        {
            // a seconda di dove mi trovo faccio in modo che il piano sia perpendicolare alla camera
            for (int x = 0; x < level.size_grid_lun; x++)
            {
                for (int y = 0; y < level.size_grid_alt; y++)
                {
                    // modifico il materiale delle celle della griglia in modo che circondino la cella target in verticale e in orizzontale
                    level.griglia[x, y, prev_z].GetComponent<MeshRenderer>().material = mat_surrounding_cell;
                }
            }
        }
        else if (direction == Camera_Directions.FACE2 || direction == Camera_Directions.FACE4)
        {
            for (int y = 0; y < level.size_grid_alt; y++)
            {
                for (int z = 0; z < level.size_grid_prof; z++)
                {
                    level.griglia[prev_x, y, z].GetComponent<MeshRenderer>().material = mat_surrounding_cell;
                }
            }
        }
        else if (direction == Camera_Directions.FACE5 || direction == Camera_Directions.FACE6)
        {
            for (int x = 0; x < level.size_grid_lun; x++)
            {
                for (int z = 0; z < level.size_grid_prof; z++)
                {
                    level.griglia[x, prev_y, z].GetComponent<MeshRenderer>().material = mat_surrounding_cell;
                }
            }
        }




        //if (camera_handle.transform.up == new Vector3(0, 1, 0) || camera_handle.transform.up == new Vector3(0, -1, 0))
        //{
        //    if (direction == Camera_Directions.FACE1 || direction == Camera_Directions.FACE2 || direction == Camera_Directions.FACE3 || direction == Camera_Directions.FACE4)
        //    {
        //        for (int x = 0; x < level.sizeGrid; x++)
        //        {
        //            for (int z = 0; z < level.sizeGrid; z++)
        //            {
        //                level.griglia[x, prev_y, z].GetComponent<MeshRenderer>().material = mat_surrounding_cell;
        //            }
        //        }
        //    }
        //}
        //else if (camera_handle.transform.up == new Vector3(1, 0, 0) || camera_handle.transform.up == new Vector3(-1, 0, 0))
        //{
        //    if (direction == Camera_Directions.FACE1 || direction == Camera_Directions.FACE3/* || direction == Camera_Directions.FACE5 || direction == Camera_Directions.FACE6*/)
        //    {
        //        for (int y = 0; y < level.sizeGrid; y++)
        //        {
        //            for (int z = 0; z < level.sizeGrid; z++)
        //            {
        //                level.griglia[prev_x, y, z].GetComponent<MeshRenderer>().material = mat_surrounding_cell;
        //            }
        //        }
        //    }
        //}
        //else if (camera_handle.transform.up == new Vector3(0, 0, 1) || camera_handle.transform.up == new Vector3(0, 0, -1))
        //{
        //    if (direction == Camera_Directions.FACE2 || direction == Camera_Directions.FACE4/* || direction == Camera_Directions.FACE5 || direction == Camera_Directions.FACE6*/)
        //    {
        //        for (int x = 0; x < level.sizeGrid; x++)
        //        {
        //            for (int y = 0; y < level.sizeGrid; y++)
        //            {
        //                level.griglia[x, y, prev_z].GetComponent<MeshRenderer>().material = mat_surrounding_cell;
        //            }
        //        }
        //    }
        //}


        //// il piano rimane orizzontale rispetto gli assi globali
        //for (int x = 0; x < level.sizeGrid; x++)
        //{
        //    for (int z = 0; z < level.sizeGrid; z++)
        //    {
        //        level.griglia[x, prev_y, z].GetComponent<MeshRenderer>().material = mat_surrounding_cell;
        //    }
        //}



        //if (camera_handle.transform.up == new Vector3(0, 1, 0) || camera_handle.transform.up == new Vector3(0, -1, 0))
        //{
        //    if (direction == Camera_Directions.FACE1 || direction == Camera_Directions.FACE2 || direction == Camera_Directions.FACE3 || direction == Camera_Directions.FACE4)
        //    {
        //        for (int x = 0; x < level.sizeGrid; x++)
        //        {
        //            for (int z = 0; z < level.sizeGrid; z++)
        //            {
        //                level.griglia[x, prev_y, z].GetComponent<MeshRenderer>().material = mat_surrounding_cell;
        //            }
        //        }
        //    }
        //}
        //else if (camera_handle.transform.up == new Vector3(1, 0, 0) || camera_handle.transform.up == new Vector3(-1, 0, 0))
        //{
        //    if (direction == Camera_Directions.FACE1 || direction == Camera_Directions.FACE3 || direction == Camera_Directions.FACE5 || direction == Camera_Directions.FACE6)
        //    {
        //        for (int y = 0; y < level.sizeGrid; y++)
        //        {
        //            for (int z = 0; z < level.sizeGrid; z++)
        //            {
        //                level.griglia[prev_x, y, z].GetComponent<MeshRenderer>().material = mat_surrounding_cell;
        //            }
        //        }
        //    }
        //}
        //else if (camera_handle.transform.up == new Vector3(0, 0, 1) || camera_handle.transform.up == new Vector3(0, 0, -1))
        //{
        //    if (direction == Camera_Directions.FACE2 || direction == Camera_Directions.FACE4 || direction == Camera_Directions.FACE5 || direction == Camera_Directions.FACE6)
        //    {
        //        for (int x = 0; x < level.sizeGrid; x++)
        //        {
        //            for (int y = 0; y < level.sizeGrid; y++)
        //            {
        //                level.griglia[x, y, prev_z].GetComponent<MeshRenderer>().material = mat_surrounding_cell;
        //            }
        //        }
        //    }
        //}
    }

    //_____________________________________________________________________________________________________

    void Reset_Plane_Material()
    {
        for (int x = 0; x < level.size_grid_lun; x++)
        {
            for (int y = 0; y < level.size_grid_alt; y++)
            {
                for (int z = 0; z < level.size_grid_prof; z++)
                {
                    if (level.griglia[x, y, z].GetComponent<MeshRenderer>().material != mat_non_target_cell)
                    {
                        level.griglia[x, y, z].GetComponent<MeshRenderer>().material = mat_non_target_cell;
                    }
                }
            }
            
        }
    }

    //_____________________________________________________________________________________________________

    bool Check_Coordinate_Max(int coordinate, int max_value)
    {
        // controllo che l'indice passato (coordinate), aumentato di uno, non superi il MASSIMO valore che può raggiungere  
        if(coordinate + 1 >= max_value)
        {
            return true;
        }
        return false;
    }

    //_____________________________________________________________________________________________________

    bool Check_Coordinate_Min(int coordinate)
    {
        // controllo che l'indice passato (coordinate), diminuito di uno, non superi il minimo valore che può raggiungere
        if (coordinate - 1 < 0)
        {
            return true;
        }
        return false;
    }
}
