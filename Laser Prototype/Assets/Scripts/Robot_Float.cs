﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robot_Float : MonoBehaviour {

    public float floating_speed = 1f;
    public float floating_amplitude = 1;
    public float rotation_speed = 0f;

    Vector3 pos_offset = new Vector3();
    Vector3 temp_pos = new Vector3();
    // Use this for initialization
    void Start()
    {
        pos_offset = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {

        //transform.Rotate(new Vector3(0f, Time.deltaTime * rotation_speed, 0f), Space.World);

        temp_pos = pos_offset;

        temp_pos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * floating_speed) * floating_amplitude;

        transform.localPosition = temp_pos;
    }
}
