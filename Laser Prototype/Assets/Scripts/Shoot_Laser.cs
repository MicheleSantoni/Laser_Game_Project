﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot_Laser : MonoBehaviour {

    public LineRenderer line;
    //public LineRenderer laser_prime;
    Ray ray;
    RaycastHit hit;
    GameObject last_hitted;
    

	// Use this for initialization
	void Start () {
        ray = new Ray();
        hit = new RaycastHit();
        
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        ray.origin = transform.position;
        ray.direction = transform.right;

        line.SetPosition(0, transform.position);
        line.SetPosition(1, transform.position + (transform.right * 10));

        //print("\nline.GetPosition(0) " + line.GetPosition(0) + "\nline.GetPosition(1) " + line.GetPosition(1));
        if (line.enabled)
        {
            if (Physics.Raycast(ray, out hit, float.MaxValue, 1 << 8))
            {
                line.SetPosition(1, hit.point);

                if (hit.collider != null)
                {

                    if (hit.collider.tag == "Player")
                    {
                        hit.collider.gameObject.GetComponent<Activate_Laser>().Activate();
                        last_hitted = hit.collider.gameObject;
                    }
                    else
                    {
                        if (last_hitted)
                        {

                            last_hitted.GetComponent<Activate_Laser>().Deactivate();
                            last_hitted = null;
                            print("stop 2");
                        }
                    }
                }
            }
            //StartCoroutine(Shoot());
        }
        if (!Physics.Raycast(ray, out hit, float.MaxValue, 1 << 8))
        {
            
            if (last_hitted)
            {
                last_hitted.GetComponent<Activate_Laser>().Deactivate();
                last_hitted = null;
                print("stop 1");
            }
        }
        Debug.DrawRay(ray.origin, ray.direction * 10, Color.blue);
        
            
    }

    //IEnumerator Shoot()
    //{
    //    while (Physics.Raycast(ray, out hit, float.MaxValue, 1 << 8))
    //    {

    //        //Debug.DrawRay(ray.origin, ray.direction * 10);
    //        //Debug.Log(last_hitted);
    //        line.SetPosition(1, hit.point);

    //        if (hit.collider != null)
    //        {
                
    //            if (hit.collider.tag == "Player")
    //            {
    //                hit.collider.gameObject.GetComponent<Activate_Laser>().Activate();
    //                last_hitted = hit.collider.gameObject;
    //            }
    //            else
    //            {
    //                if (last_hitted)
    //                {
                            
    //                    last_hitted.GetComponent<Activate_Laser>().Deactivate();
    //                    last_hitted = null;
    //                    print("stop 2");
    //                }
    //            }
    //        }

    //        yield return null;
    //    }
    //}
}
