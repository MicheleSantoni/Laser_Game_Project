﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour {

    GameObject laser;

	// Use this for initialization
	void Start () {
        laser = GameObject.Find("Laser Emitter");
        laser.GetComponent<Laser>().is_shooting = true;
    }
}
