﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Sound_Event : ScriptableObject {

    private List<Sound_Event_Listener> listeners = new List<Sound_Event_Listener>();

    public void Raise()
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised();
        }
    }

    public void RegisterListener(Sound_Event_Listener listener)
    {
        listeners.Add(listener);
    }

    public void UnregisterListener(Sound_Event_Listener listener)
    {
        listeners.Remove(listener);
    }
}
