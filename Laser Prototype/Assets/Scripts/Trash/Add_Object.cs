﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Add_Object : MonoBehaviour {

    public GameObject preload;
    public GameObject load;
    GameObject pre_object;

    private void Update()
    {
        Posiz_Oggetto();
    }

    void Posiz_Oggetto()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << 9))
        {
            if(hit.collider != null)
            {
                //print("trovato");
                //print(pre_object);
                Vector3 obj_position = new Vector3(hit.collider.transform.position.x, hit.collider.transform.position.y + 0.5f, hit.collider.transform.position.z);

                if (pre_object == null)
                {
                    pre_object = Instantiate(preload, obj_position, Quaternion.identity);
                }
                else
                {
                    pre_object.transform.position = obj_position;
                }

                if (Input.GetMouseButtonDown(0))
                {
                    GameObject positioned_object = Instantiate(load, obj_position, Quaternion.identity);
                    //hit.collider.GetComponent<MeshRenderer>().enabled = false;
                }
            }
        }
    }
}
