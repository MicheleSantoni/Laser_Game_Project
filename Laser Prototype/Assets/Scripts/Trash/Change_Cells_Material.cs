﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Change_Cells_Material : MonoBehaviour {

    public Material mat_non_target_cell;
    public Material mat_target_cell;
    public Material mat_selected_cell;
    public Material mat_surrounding_cell;

    
}
