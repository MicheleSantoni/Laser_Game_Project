﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Game_Control : MonoBehaviour {

    //   public enum Actions {POSIZIONAMENTO, SELEZIONE, ROTAZIONE};
    //   public Actions action;

    //   GameObject selected_object;
    //   public Material chosen;
    //   public Material unchosen;

    //   public Text text_action;

    //   // Use this for initialization
    //   void Start () {
    //       action = Actions.POSIZIONAMENTO;

    //       Cambia_Testo_Azione();
    //   }

    //// Update is called once per frame
    //void Update () {

    //       Cambia_Azione();

    //       switch(action)
    //       {
    //           case Actions.POSIZIONAMENTO:
    //               break;

    //           case Actions.SELEZIONE:
    //               Selezione_Oggetto();
    //               break;

    //           case Actions.ROTAZIONE:
    //               Roteazione_Oggetto();
    //               break;

    //           default:
    //               break;
    //       }

    //       //print(action);
    //       Cambia_Testo_Azione();
    //   }

    //   void Selezione_Oggetto()
    //   {
    //       Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    //       RaycastHit hitInfo = new RaycastHit();

    //       if (Input.GetButtonUp("Fire1"))
    //       {
    //           if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity))
    //           {
    //               if (hitInfo.collider.tag == "Glass")
    //               {
    //                   //print("Beccato");
    //                   selected_object = hitInfo.collider.gameObject;
    //                   selected_object.GetComponent<MeshRenderer>().material = chosen;
    //                   action = Actions.ROTAZIONE;
    //               }
    //               //print(hitInfo.collider.name);
    //           }

    //       }
    //   }

    //   void Roteazione_Oggetto()
    //   {
    //       float y = Input.GetAxis("Horizontal");
    //       float x = Input.GetAxis("Vertical");

    //       if (selected_object)
    //       {
    //           selected_object.transform.Rotate(Vector3.up * y);
    //           selected_object.transform.Rotate(Vector3.right * x);
    //       }
    //   }

    //   void Cambia_Azione()
    //   {
    //       if (Input.GetKeyDown(KeyCode.Space))
    //       {
    //           if (action == Actions.ROTAZIONE)
    //           {
    //               selected_object.GetComponent<MeshRenderer>().material = unchosen;
    //               action = 0;
    //           }
    //           else
    //           {
    //               action++;
    //           }
    //       }
    //   }

    //void Cambia_Testo_Azione()
    //{
    //    text_action.text = "Azione " + action.ToString();
    //}

    private void Update()
    {
        Debug();
    }

    public void ControlloVittoria()
    {
        GameObject[] go = GameObject.FindGameObjectsWithTag("Collectable");
        int counter = 0;
        foreach (GameObject gos in go)
        {
            if (gos.GetComponent<CollectableScript>().GetTouched())
                counter++;
        }

        if (counter == go.Length)
        {
            print("Vittoria");
            Text winner = GameObject.Find("Winner").GetComponent<Text>();
            winner.enabled = true;
        }
    }

    void Debug()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            //print(SceneManager.GetActiveScene().buildIndex);
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
