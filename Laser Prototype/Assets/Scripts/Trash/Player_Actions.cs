﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Actions : MonoBehaviour {

    public GameObject preload;
    public GameObject load;
    GameObject pre_object;
    GameObject camera_ctrl;
    GameObject piano;
    GameObject selected_object;
    Change_Action.Actions actual_action;
    List<GameObject> listScriptable;//Cini change
    LevelCreate level;
    

    void Start()
    {
        piano = GameObject.Find("Piano");
        camera_ctrl = GameObject.Find("Camera Controller");
        listScriptable = new List<GameObject>();
    }

    //private void Update()
    //{
        
    //    actual_action = GameObject.Find("Game Manager").GetComponent<Change_Action>().action;

    //    camera_ctrl.GetComponent<Camera_Controller>().Rotate_Camera();

    //    switch (actual_action)
    //    {
    //        case Change_Action.Actions.POSITION:
    //            Set_Selected_Object_Null();
    //            piano.GetComponent<Control_Plane>().Set_Grid_On();
    //            Posiz_Oggetto();
    //           // piano.GetComponent<Control_Plane>().Change_Piano_Game(); Cini Change
    //            break;

    //        case Change_Action.Actions.SELECTION:
    //            if(pre_object)
    //            {
    //                Destroy(pre_object);
    //            }
    //            piano.GetComponent<Control_Plane>().Set_Grid_Off();
    //            Selez_Oggetto();
    //            break;

    //        case Change_Action.Actions.ROTATION:
    //            {

    //            }
    //            break;

    //        default:
    //            break;
                
    //    }
    //    if (Input.GetKey(KeyCode.F))
    //    {
          
    //    }
       
       
    //}

    void Posiz_Oggetto()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << 9))
        {
            if (hit.collider != null)
            {
                //print("trovato");
                //print(pre_object);
                Vector3 obj_position = new Vector3(hit.collider.transform.position.x, hit.collider.transform.position.y + 0.5f, hit.collider.transform.position.z);

                if (pre_object == null)
                {
                    pre_object = Instantiate(preload, obj_position, Quaternion.identity);
                    
                }
                else
                {
                    pre_object.transform.position = obj_position;
                }

                if (Input.GetMouseButtonDown(0))
                {
                    
                    GameObject positioned_object = Instantiate(load, obj_position, Quaternion.identity);
                    
                    
                   
                }
            }
        }
    }

    void Selez_Oggetto()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();

        
        if (Input.GetMouseButtonDown(0))
        {
            print(selected_object);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << 8))
            {
                if (hit.collider != null)
                {
                    if (hit.collider.gameObject != selected_object)
                    {
                        Set_Selected_Object_Null();
                    }
                    selected_object = hit.collider.gameObject;

                    selected_object.GetComponent<Cube_Movement>().enabled = true;
                }

            }
            else
            {
                Set_Selected_Object_Null();
            }
        }
    }

    void Set_Selected_Object_Null()
    {
        if (selected_object != null)
        {
            selected_object.GetComponent<Cube_Movement>().enabled = false;
            selected_object = null;
        }
    }

 
}
