﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ButtonScript : MonoBehaviour {


    float larghezza;
    float altezza;
    public float rotMaxVert, rotMaxOriz;


	// Update is called once per frame
	void Start () {
        larghezza = Camera.main.scaledPixelWidth / 2;   //x     questo è il centro
        altezza = Camera.main.scaledPixelHeight / 2;    //y

	}
    void Update()
    {
        Cursor.lockState = CursorLockMode.Confined;
        float x = Input.mousePosition.x;
        float y = Input.mousePosition.y;

        //print("X: " + x + "\nY:" + y);
        //print("Larghezza: " + larghezza + "\nAltezza: " + altezza);
        float rapX = (x - larghezza);
        float rapY = (y - altezza);

        float rotX = (rapX * rotMaxOriz) / larghezza;

        float rotY = (rapY * rotMaxVert) / altezza;


        gameObject.transform.rotation = Quaternion.Euler(rotY, -rotX, this.transform.rotation.z);
        

    }

}
