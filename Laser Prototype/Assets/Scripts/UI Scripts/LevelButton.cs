﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
[RequireComponent(typeof(Button))]
public class LevelButton : MonoBehaviour {
    Text text;
    Button button;
    string levelName;
    public LevelsScriptable Levels;

	// Use this for initialization
	void Start () {

        if (Levels.levels.Length <= transform.GetSiblingIndex())
        {
            Destroy(gameObject);
        }
        else
        {

            button = GetComponent<Button>();
            text = GetComponentInChildren<Text>();
           // text.text = Levels.levels[transform.GetSiblingIndex()];
            levelName = Levels.levels[transform.GetSiblingIndex()];
        }
	}

    public void TaskOnClick()
    {
        print("Loading di " + levelName);
        SceneManager.LoadScene(levelName);
    }

}
