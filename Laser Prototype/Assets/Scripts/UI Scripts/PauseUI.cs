﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseUI : MonoBehaviour {

    public Button Quit, Resume;
    public Image immagine;

	
	// Update is called once per frame
	void Update () {
		
        if(Input.GetKeyDown(KeyCode.P))
        {
            VisibleAll();
        }

	}

    void VisibleAll()
    {
        Quit.gameObject.SetActive(true);
        Resume.gameObject.SetActive(true);
        immagine.gameObject.SetActive(true);
    }

    public void InvisibleAll()
    {
        Quit.gameObject.SetActive(false);
        Resume.gameObject.SetActive(false);
        immagine.gameObject.SetActive(false);
    }

}
