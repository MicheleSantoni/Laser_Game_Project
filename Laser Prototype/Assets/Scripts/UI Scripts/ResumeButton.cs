﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResumeButton : MonoBehaviour {

    public Button button;
    public PauseUI pauseCanvas;

    void Start()
    {
        Button btn = button.GetComponent<Button>();
        btn.onClick.AddListener(DoResume);
    }

    void DoResume()
    {
        pauseCanvas.InvisibleAll();
    }

}
