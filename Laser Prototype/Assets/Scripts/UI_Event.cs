﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class UI_Event : ScriptableObject {

    private List<UI_Event_Listener> listeners = new List<UI_Event_Listener>();

    public void Raise()
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised();
        }
    }

    public void RegisterListener(UI_Event_Listener listener)
    {
        listeners.Add(listener);
    }

    public void UnregisterListener(UI_Event_Listener listener)
    {
        listeners.Remove(listener);
    }
}
